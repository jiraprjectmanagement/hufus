
//   Home Page JS

$(function () {

   

$('.counter-txt').countUp();

//owlCarousel slider  
var owl = $(".video-slider-1").owlCarousel({
    items: 1,
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 4000,
    //autoplayHoverPause: true,
    lazyLoad: true,
    dots: true,
    onTranslate: function () {
        $('.owl-item').find('video').each(function () {
            this.pause();
        });
    }
});

// Play slider when video pause
function playSlider(){
    window.owl.trigger('play.owl.autoplay')
}

// Pause slider when video play
function pauseSlider(){
    window.owl.trigger('stop.owl.autoplay')
}


$('.main-banner').owlCarousel({
    loop:false,
    nav: true,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    dots: false,
    margin:30,
    animateOut: 'fadeOut',
    autoplay:true,
    autoplayTimeout:15000,
    autoplayHoverPause:false,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            dots: false,
        },                   
        574:{
            items:1,
            dots: false,
        },
        766: {
            items: 1,
            dots: false,
        }, 
        992: {
            items: 1,
            dots: false,
        }
    }
});


$('.community-slider').owlCarousel({
    loop:false,
    nav: false,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    dots: false,
    margin:30,
    stagePadding: 5,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav: true,
            dots:false
        },                   
        576:{
            items:2,
            nav: true,
            dots:false
        },
        768: {
            items: 2,
            dots: false,
            margin:30,
        }, 
        992: {
            items: 2,
            dots: false,
            margin:30,
        }, 
        1199: {
            items: 4,
            dots: true
        }
    }
});

   


if($(window).width() <= 768){
    if(('.coomon-responsove-slider').length != 0){
        $('.coomon-responsove-slider').addClass('owl-carousel owl-theme');
        $('.coomon-responsove-slider').owlCarousel({
            loop:false,
            margin:0,
            dots: true,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1
                }, 
                480:{

                    items:1
                },
                577:{

                    items:1
                },
                767:{
                    items:1
                }
            }
        });
    }
}

if($(window).width() <= 768){
    if(('.coomon-responsove-impact-slider').length != 0){
        $('.coomon-responsove-impact-slider').addClass('owl-carousel owl-theme');
        $('.coomon-responsove-impact-slider').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:true,
                    dots: false,
                }, 
                480:{

                    items:1,
                    nav:true,
                    dots: false,
                },
                577:{

                    items:1,
                    nav:true,
                    dots: false,
                },
                767:{
                    items:2
                }
            }
        });
    }
}


if($(window).width() <= 993){
    if(('.coomonboard-responsove-impact-slider').length != 0){
        $('.coomonboard-responsove-impact-slider').addClass('owl-carousel owl-theme');
        $('.coomonboard-responsove-impact-slider').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1
                }, 
                480:{

                    items:1
                },
                574:{

                    items:2
                },
                767:{
                    items:3
                },
                992:{
                    items:4
                }
            }
        });
    }
}


$(window).scroll(function(){
    var scrollheader  = $(window).scrollTop() > 50;
    
    if(scrollheader){
        $(".main-header").addClass("header-sticky");
    }
    else{
        $(".main-header").removeClass("header-sticky");
    }
   
});

$(window).scroll(function(){
    var scrollheader  = $(window).scrollTop() > 1500;
    console.log(scrollheader);
    if(scrollheader){
        $(".load-anim").addClass("anima-loa-active");
        // $(".count-info h3").removeClass("deflt-spin");
        
    }
    else{
        $(".load-anim").removeClass("anima-loa-active");
        // $(".count-info h3").addClass("deflt-spin");
    }
});


$(document).ready(function(){
	$('#nav-icon3').click(function(){
		$(this).toggleClass('open');
	});
});




AOS.init({
    disable: function() {
        return window.innerWidth < 800;
    }
});



});
